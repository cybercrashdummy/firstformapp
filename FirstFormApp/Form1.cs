﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FirstFormApp
{
    public partial class Form1 : Form
    {
        public int iGuessCount = 0;
        bool bG = false, bI = false, bR = false, bA = false;
        bool bF = false, bE = false;

        private void Label11_Click(object sender, EventArgs e)
        {

        }

        private void TextBox30_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        public Form1()
        {
            InitializeComponent();
        }

        public void TextBox55_TextChanged(object sender, EventArgs e)
        {
            
            /*
             
             */
        }

        private void GOButton_Click(object sender, EventArgs e)
        {
            string cSecretWord = "giraffe", cGuess = "", cSubGuess = "";
            cGuess = GuessInput.Text;
            int iGuessLength;
            int iposition = 0;
            int iCorrectCount = 0;
            iGuessLength = cGuess.Length;
            label2.Left = GuessInput.Left + ((GuessInput.Width - label2.Width) / 2);
            while (iposition < iGuessLength)
            {
                cSubGuess = cGuess.Substring(iposition, 1);

                switch (cSubGuess)
                {
                    case "g":
                        textBox16.Text = "G";
                        iposition++;
                        iCorrectCount++;
                        bG = true;
                        break;

                    case "i":
                        textBox17.Text = "I";
                        iposition++;
                        iCorrectCount++;
                        bI = true;
                        break;

                    case "r":
                        textBox18.Text = "R";
                        iposition++;
                        iCorrectCount++;
                        bR = true;
                        break;

                    case "a":
                        textBox19.Text = "A";
                        iposition++;
                        iCorrectCount++;
                        bA = true;
                        break;

                    case "f":
                        textBox20.Text = "F";
                        textBox21.Text = "F";
                        iposition++;
                        iCorrectCount++;
                        bF = true;
                        break;

                    case "e":
                        textBox22.Text = "E";
                        iposition++;
                        iCorrectCount++;
                        bE = true;
                        break;

                    default:
                        iposition++;
                        break;
                }
            }
            iGuessCount++;
            if (iGuessCount != 1)
            {
                if (!bG || !bI || !bR || !bA || !bF || !bE)
                {
                    label3.Text = iGuessCount + " Tries";
                }
                else
                {
                    label3.Text = ("You Guessed it in " + iGuessCount + " Tries");
                }
            }
            else
            {
                if (!bG || !bI || !bR || !bA || !bF || !bE)
                {
                    label3.Text = iGuessCount + " Try";
                }
                else
                {
                    label3.Text = ("You Guessed it in " + iGuessCount + " Try");
                }
            }
            label3.Left = GuessInput.Left + ((GuessInput.Width - label3.Width) / 2);
            GuessInput.Text = "";
        }

        private void TextBox25_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

